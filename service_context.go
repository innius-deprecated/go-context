package context

import (
	gocontext "context"
	"net/http"
	"net/url"
	"strings"
	"time"

	"bitbucket.org/to-increase/go-auth/claims"
	sdktypes "bitbucket.org/to-increase/go-sdk/types"
	log "bitbucket.org/to-increase/go-logger"
	"bitbucket.org/to-increase/go-trn/uuid"
	"github.com/labstack/echo"
)

type SvcContext interface {
	// Get the logger of this context object
	Logger() *log.Logger
	// Return the echo.Context contain within this service context
	EchoContext() echo.Context
	// Does the SvcContext have an echo context?
	HasEchoContext() bool
	// Retrieve the conversation ID header, or generate a new one automatically.
	GetRequestIDHeader() (string, string)

	// Retrieve the log level header
	GetLogLevelHeader() (string, string)

	// Derive a TaskContext from a SvcContext
	DeriveTaskContext() TaskContext

	// GetToken returns the the bearer token of the request
	GetToken() string

	// Echo compatibility

	Request() *http.Request
	Response() *echo.Response
	Path() string
	Param(name string) (value string)
	Query(name string) string
	Form(name string) string
	Get(key string) interface{}
	Set(key string, val interface{})
	Bind(i interface{}) error
	Render(code int, name string, data interface{}) (err error)
	HTML(code int, html string) (err error)
	String(code int, s string) (err error)
	JSON(code int, i interface{}) (err error)
	JSONBlob(code int, b []byte) (err error)
	// NOTE no longer supported by echo
	// JSONIndent(code int, i interface{}, prefix string, indent string) (err error)
	JSONP(code int, callback string, i interface{}) (err error)
	XML(code int, i interface{}) (err error)
	// NOTE no longer supported by echo
	// XMLIndent(code int, i interface{}, prefix string, indent string) (err error)
	Attachment(file string, name string) (err error)
	NoContent(code int) error
	Redirect(code int, url string) error
	Error(err error)
	Echo() *echo.Echo
	Claims() *claims.Claims
	WithToken(token string) SvcContext

	// standard go context implementation
	gocontext.Context
}

type context struct {
	echo.Context
	log     *log.Logger
	reqid   string
	claims  *claims.Claims
	token   string
	context gocontext.Context
}

func NewCtxFromEchoWithName(name string, e echo.Context) SvcContext {
	if e != nil {
		return &context{
			Context: e,
			log:     log.GetLogEntryForContext(e).WithField("name", name),
			context: e.Request().Context(),
		}
	} else {
		return &context{
			Context: nil,
			log:     log.NewScopedLogger(),
			context: gocontext.Background(),
		}
	}
}

func NewCtxFromEcho(e echo.Context) SvcContext {
	return NewCtxFromEchoWithName("no name provided", e)
}

func NewCtx() SvcContext {
	return NewCtxFromEcho(nil)
}

func NewCtxWithClaims(claims *claims.Claims) SvcContext {
	return &context{
		Context: nil,
		log:     log.NewScopedLogger(),
		claims:  claims,
		context: gocontext.Background(),
	}
}

func (c *context) EchoContext() echo.Context {
	return c.Context
}

func (c *context) Logger() *log.Logger {
	return c.log
}

func (c *context) HasEchoContext() bool {
	return nil != c.EchoContext()
}

func (c *context) GetRequestIDHeader() (string, string) {
	header_name := "X-Request-Id"

	if c.reqid != "" {
		return header_name, c.reqid
	}

	var _new = func() (string, string) {
		c.reqid = uuid.NewV4().String()
		return header_name, c.reqid
	}

	if !c.HasEchoContext() {
		return _new()
	} else {
		if r := c.Request(); r != nil {
			if reqID := r.Header.Get(header_name); reqID != "" {
				c.reqid = reqID
				return header_name, reqID
			}
		}
	}
	return _new()
}

//GetToken returns the bearer token from the current request
func (c *context) GetToken() string {
	if c.token != "" {
		return c.token
	}
	headerName := http.CanonicalHeaderKey("authorization")
	if c.HasEchoContext() {
		if r := c.Request(); r != nil {
			if ah := r.Header.Get(headerName); ah != "" {
				// Should be a bearer token
				if len(ah) > 6 && strings.ToUpper(ah[0:6]) == "BEARER" {
					return ah[7:]
				}
			}
			if cookie, _ := c.EchoContext().Cookie("access_token"); cookie != nil {
				return cookie.Value
			}

		}
	}
	return ""
}

func (c *context) GetLogLevelHeader() (string, string) {
	header_name := http.CanonicalHeaderKey("x-log-level")

	if !c.HasEchoContext() {
		return "", ""
	} else {
		if r := c.Request(); r != nil {
			if level := r.Header.Get(header_name); level != "" {
				return header_name, level
			}
		}
	}
	return "", ""
}

func (c *context) DeriveTaskContext() TaskContext {
	return c
}

func (c *context) Claims() *claims.Claims {
	if c.HasEchoContext() {
		return claims.FromEchoContext(c.EchoContext())
	} else {
		return c.claims
	}
}

// Mirror the whole echo.Context object so we are API-compatible.
// However, do so in a safe way so that we don't panic.

// Request returns *http.Request.
func (c *context) Request() *http.Request {
	if c.HasEchoContext() {
		return c.EchoContext().Request()
	} else {
		return nil
	}
}

// Response returns *Response.
func (c *context) Response() *echo.Response {
	if c.HasEchoContext() {
		return c.EchoContext().Response()
	} else {
		return nil
	}
}

// Socket returns *websocket.Conn.
/* NOTE no longer supported by echo
func (c *context) Socket() *websocket.Conn {
	if c.HasEchoContext() {
		return c.EchoContext().Socket()
	} else {
		return nil
	}
}
*/

// Path returns the http request path (NOT the echo path)
func (c *context) Path() string {
	if c.HasEchoContext() {
		return c.EchoContext().Request().URL.Path
	} else {
		return ""
	}
}

// Param returns path parameter by name.
func (c *context) Param(name string) string {
	if c.HasEchoContext() {
		v, _ := url.PathUnescape(c.EchoContext().Param(name))
		return v
	}
	return ""
}

// Query returns query parameter by name.
func (c *context) Query(name string) string {
	if c.HasEchoContext() {
		v, _ := url.QueryUnescape(c.EchoContext().QueryParam(name))
		return v
	}
	return ""
}

// Form returns form parameter by name.
func (c *context) Form(name string) string {
	if c.HasEchoContext() {
		return c.EchoContext().FormValue(name)
	} else {
		return ""
	}
}

// Get retrieves data from the context.
func (c *context) Get(key string) interface{} {
	if c.HasEchoContext() {
		return c.EchoContext().Get(key)
	} else {
		return nil
	}
}

// Set saves data in the context.
func (c *context) Set(key string, val interface{}) {
	if c.HasEchoContext() {
		c.EchoContext().Set(key, val)
	}
}

// Bind binds the request body into specified type `i`. The default binder does
// it based on Content-Type header.
func (c *context) Bind(i interface{}) error {
	if c.HasEchoContext() {
		return c.EchoContext().Bind(i)
	} else {
		return nil
	}
}

// The following functions do assume an echo object. Since they create response, it makes no sense to just ignore the
// fact that we don't have a valid echo context.

// Render renders a template with data and sends a text/html response with status
// code. Templates can be registered using `Echo.SetRenderer()`.
func (c *context) Render(code int, name string, data interface{}) error {
	return c.EchoContext().Render(code, name, data)
}

// HTML sends an HTTP response with status code.
func (c *context) HTML(code int, html string) error {
	return c.EchoContext().HTML(code, html)
}

// String sends a string response with status code.
func (c *context) String(code int, s string) error {
	return c.EchoContext().String(code, s)
}

// JSON sends a JSON response with status code.
func (c *context) JSON(code int, i interface{}) error {
	return c.EchoContext().JSON(code, i)
}

// JSONIndent sends a JSON response with status code, but it applies prefix and indent to format the output.
/* NOTE no longer supported by Echo
func (c *context) JSONIndent(code int, i interface{}, prefix string, indent string) error {
	return c.EchoContext().JSONIndent(code, i, prefix, indent)
}
*/

// JSONP sends a JSONP response with status code. It uses `callback` to construct
// the JSONP payload.
func (c *context) JSONP(code int, callback string, i interface{}) error {
	return c.EchoContext().JSONP(code, callback, i)
}

// XML sends an XML response with status code.
func (c *context) XML(code int, i interface{}) error {
	return c.EchoContext().XML(code, i)
}

// XMLIndent sends an XML response with status code, but it applies prefix and indent to format the output.
/* NOTE no longer supported by Echo
func (c *context) XMLIndent(code int, i interface{}, prefix string, indent string) error {
	return c.EchoContext().XMLIndent(code, i, prefix, indent)
}
*/

// Attachment sends specified file as an attachment to the client.
func (c *context) Attachment(file string, name string) error {
	return c.EchoContext().Attachment(file, name)
}

// NoContent sends a response with no body and a status code.
func (c *context) NoContent(code int) error {
	return c.EchoContext().NoContent(code)
}

// Redirect redirects the request using http.Redirect with status code.
func (c *context) Redirect(code int, url string) error {
	return c.EchoContext().Redirect(code, url)
}

// Error invokes the registered HTTP error handler. Generally used by middleware.
func (c *context) Error(err error) {
	c.EchoContext().Error(err)
}

// Echo returns the `Echo` instance.
func (c *context) Echo() *echo.Echo {
	if c.HasEchoContext() {
		return c.EchoContext().Echo()
	} else {
		return nil
	}
}

// returns a new service context with the token embedded
func (c *context) WithToken(token string) SvcContext {
	clone := &context{
		Context: c.EchoContext(),
		log:     c.Logger(),
		reqid:   c.reqid,
		token:   token,
		context: gocontext.WithValue(c.context, sdktypes.TokenContextKey, token),
	}
	return clone
}

// standard go context interface
func (c *context) Deadline() (deadline time.Time, ok bool) {
	return c.context.Deadline()
}

func (c *context) Done() <-chan struct{} {
	return c.context.Done()
}

func (c *context) Err() error {
	return c.context.Err()
}

func (c *context) Value(key interface{}) interface{} {
	return c.context.Value(key)
}
