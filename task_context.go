package context

import (
	gocontext "context"
	"net/http"
	"time"

	stdcontext "context"

	log "bitbucket.org/to-increase/go-logger"
	"bitbucket.org/to-increase/go-trn/uuid"
)

func NewTaskContextBuilder() *taskContextBuilder {
	return &taskContextBuilder{}
}

type taskContextBuilder struct {
	requestID string
	traceid   string
	loglevel  string
	token     string
	log       *log.Logger
}

// Sets the request id for downstream service requests
func (tcb *taskContextBuilder) WithRequestID(requestID string) *taskContextBuilder {
	tcb.requestID = requestID
	return tcb
}

// Sets the request log level for downstream service requests
func (tcb *taskContextBuilder) WithLogLevel(loglevel string) *taskContextBuilder {
	tcb.loglevel = loglevel
	return tcb
}

func (tcb *taskContextBuilder) WithToken(token string) *taskContextBuilder {
	tcb.token = token
	return tcb
}

func (tcb *taskContextBuilder) WithTraceID(traceid string) *taskContextBuilder {
	tcb.traceid = traceid
	return tcb
}

func (tcb *taskContextBuilder) WithLogger(l *log.Logger) *taskContextBuilder {
	tcb.log = l
	return tcb
}

func (tcb *taskContextBuilder) Build() TaskContext {
	tc := &task_context{
		store:     make(map[string]interface{}),
		log:       log.CreateLogger().WithField("TaskContext", true),
		xloglevel: tcb.loglevel,
		xreqid:    tcb.requestID,
		token:     tcb.token,
		context:   gocontext.Background(),
	}

	if tcb.log != nil {
		tc.log = tcb.log
	}
	if tcb.loglevel != "" {
		tc.Logger().SetLevel(tcb.loglevel)
	}

	return tc
}

type TaskContext interface {
	// Get the logger of this context object
	Logger() *log.Logger

	// Retrieve the conversation ID header, or generate a new one automatically.
	GetRequestIDHeader() (string, string)

	// Retrieve the log level header
	GetLogLevelHeader() (string, string)

	// GetToken returns the the bearer token of the request
	GetToken() string

	Get(key string) interface{}
	Set(key string, val interface{})

	// standard go context implementation
	gocontext.Context
}

type task_context struct {
	store     map[string]interface{}
	log       *log.Logger
	xreqid    string
	xloglevel string
	token     string
	context   gocontext.Context
}

type tokenContextKey struct{}

// GetToken returns the token from a std golang context
func GetToken(c stdcontext.Context) string {
	v := c.Value(tokenContextKey{})
	s, ok := v.(string)
	if ok {
		return s
	}
	return ""
}

//WithToken returns a new standard golang context with the provided token
func WithToken(c stdcontext.Context, token string) stdcontext.Context {
	return stdcontext.WithValue(c, tokenContextKey{}, token)
}

func NewTaskCtx() TaskContext {
	return NewTaskCtxFromContext(gocontext.Background())
}

func NewTaskCtxFromContext(ctx gocontext.Context) TaskContext {
	return &task_context{
		store:   make(map[string]interface{}),
		log:     log.CreateLogger().WithField("TaskContext", true),
		token:   GetToken(ctx),
		context: ctx,
	}
}

func (c *task_context) Logger() *log.Logger {
	return c.log
}

func (c *task_context) GetRequestIDHeader() (string, string) {
	if c.xreqid == "" {
		c.xreqid = uuid.NewV4().String()
	}
	return http.CanonicalHeaderKey("x-request-id"), c.xreqid
}

func (c *task_context) GetLogLevelHeader() (string, string) {
	if c.xloglevel == "" {
		return "", ""
	}
	return http.CanonicalHeaderKey("x-log-level"), c.xloglevel
}

func (c *task_context) GetToken() string {
	return c.token
}

// Get retrieves data from the context.
func (c *task_context) Get(key string) interface{} {
	return c.store[key]
}

// Set saves data in the context.
func (c *task_context) Set(key string, val interface{}) {
	c.store[key] = val
}

// standard go context interface
func (c *task_context) Deadline() (deadline time.Time, ok bool) {
	return c.context.Deadline()
}

func (c *task_context) Done() <-chan struct{} {
	return c.context.Done()
}

func (c *task_context) Err() error {
	return c.context.Err()
}

func (c *task_context) Value(key interface{}) interface{} {
	return c.context.Value(key)
}
