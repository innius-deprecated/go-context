package context_test

import (
	"strings"
	"testing"

	"net/http"
	"net/http/httptest"

	"bitbucket.org/to-increase/go-context"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
)

func NewEchoContext() echo.Context {
	e := echo.New()
	req := httptest.NewRequest(echo.POST, "/", strings.NewReader("{}"))
	rec := httptest.NewRecorder()
	ectx := e.NewContext(req, rec)
	return ectx
}

func TestEchoCompatibility(t *testing.T) {
	echo_ctx := NewEchoContext()
	ctx := context.NewCtxFromEcho(echo_ctx)
	var f echo.Context = ctx.EchoContext()

	assert.NotNil(t, f.Request())
	assert.NotNil(t, f.Response())
}

func TestLoggerExistence(t *testing.T) {
	e := NewEchoContext()

	assert.NotNil(t, e.Request())
	assert.NotNil(t, e.Response())

	ctx := context.NewCtxFromEcho(e)

	assert.NotNil(t, ctx.Logger())
}

func TestLoggerExistenceWithoutEcho(t *testing.T) {
	ctx := context.NewCtx()

	assert.NotNil(t, ctx.Logger())
	assert.False(t, ctx.HasEchoContext())
}

func TestRequestIdHeaderGeneration(t *testing.T) {
	ctx := context.NewCtx()
	a, b := ctx.GetRequestIDHeader()
	assert.Equal(t, a, "X-Request-Id")
	assert.NotEqual(t, b, "")
}

func TestRequestIdHeaderGenerationShouldBeDoneOnce(t *testing.T) {
	ctx := context.NewCtx()
	_, b := ctx.GetRequestIDHeader()
	_, c := ctx.GetRequestIDHeader()
	assert.Equal(t, b, c)
}

func TestLoggerNoPanicWithoutEcho(t *testing.T) {
	ctx := context.NewCtx()
	ctx.Request()
}

func TestGetToken(t *testing.T) {
	e := NewEchoContext()
	e.Request().Header.Add("Authorization", "bearer xxx")
	ctx := context.NewCtxFromEcho(e)
	token := ctx.GetToken()

	assert.NotEmpty(t, token)
}

func TestGetTokenFromCookie(t *testing.T) {
	e := NewEchoContext()

	cookie := new(http.Cookie)
	cookie.Name = "access_token"
	cookie.Value = "token"
	e.Request().AddCookie(cookie)

	ctx := context.NewCtxFromEcho(e)
	token := ctx.GetToken()

	assert.NotEmpty(t, token)
	assert.Equal(t, cookie.Value, token)
}

func TestGetTokenWithoutEcho(t *testing.T) {
	ctx := context.NewCtx()
	assert.Empty(t, ctx.GetToken())
}

func TestNewContextWithClaims(t *testing.T) {
	e := NewEchoContext()
	e.Request().Header.Add("Authorization", "bearer xxx")
	ctx := context.NewCtxFromEcho(e)
	token := "a secret service token"
	c := ctx.WithToken(token)

	assert.Equal(t, token, c.GetToken())

}
func TestGetClaims(t *testing.T) {
	e := NewEchoContext()
	ctx := context.NewCtxFromEcho(e)

	assert.NotNil(t, ctx.Claims())
	assert.NotNil(t, ctx.Claims().Principal)
}
