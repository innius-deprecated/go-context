package context_test

import (
	"testing"

	"bitbucket.org/to-increase/go-context"
	"github.com/stretchr/testify/assert"
)

func TestTaskContextGetNonExistingValue(t *testing.T) {
	ctx := context.NewTaskCtx()
	assert.Nil(t, ctx.Get("abc"))
}

func TestTaskContextSetGet(t *testing.T) {
	ctx := context.NewTaskCtx()
	ctx.Set("abc", "def")
	assert.NotNil(t, ctx.Get("abc"))
	assert.Equal(t, "def", ctx.Get("abc"))
}

func TestTaskContextTestRequestIDHeader(t *testing.T) {
	ctx := context.NewTaskCtx()
	hk, hv := ctx.GetRequestIDHeader()
	assert.NotNil(t, hk)
	assert.NotNil(t, hv)
}

func TestTaskContextBuilder(t *testing.T) {
	b := context.NewTaskContextBuilder().
		WithLogLevel("debug").WithToken("token").WithRequestID("test").Build()

	_, v := b.GetRequestIDHeader()
	assert.Equal(t, "test", v)

	_, v = b.GetLogLevelHeader()
	assert.Equal(t, "debug", v)

	assert.Equal(t, "token", b.GetToken())
}
